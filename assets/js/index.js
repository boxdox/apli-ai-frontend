// -------------------------------------------------
// Initializing chart.js
// -------------------------------------------------
const ctx = $("#profile-chart");
const chart = new Chart(ctx, {
  type: "radar",
  data: {
    labels: ["O", "C", "E", "A", "N"],
    datasets: [
      {
        data: [1, 2, 3, 4, 5],
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
          "rgba(75, 192, 192, 0.2)",
          "rgba(153, 102, 255, 0.2)"
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
          "rgba(75, 192, 192, 1)",
          "rgba(153, 102, 255, 1)"
        ],
        borderWidth: 2
      }
    ]
  },
  options: {
    legend: {
      display: false
    },
    scale: {
      angleLines: {
        display: false
      },
      ticks: {
        suggestedMin: 0,
        suggestedMax: 5,
        stepSize: 1
      }
    }
  }
});

// -------------------------------------------------
// Defining a custom list of questions
// -------------------------------------------------
const questions = [
  {
    id: 1,
    question: "This is question 1",
    video_link: "https://www.youtube.com/embed/pPwE-I2be7E"
  },
  {
    id: 2,
    question: "This is question 2",
    video_link: "https://www.youtube.com/embed/tPYj3fFJGjk"
  },
  {
    id: 3,
    question: "This is question 3",
    video_link: "https://www.youtube.com/embed/_xkSvufmjEs"
  },
  {
    id: 4,
    question: "This is question 4",
    video_link: "https://www.youtube.com/embed/U3VSJhaC4kc"
  },
  {
    id: 5,
    question: "This is question 5",
    video_link: "https://www.youtube.com/embed/LsNW4FPHuZE"
  },
  {
    id: 6,
    question: "This is question 6",
    video_link: "https://www.youtube.com/embed/RyTRgQ7k6QE"
  },
  {
    id: 7,
    question: "This is question 7",
    video_link: "https://www.youtube.com/embed/GytUZLK4kwA"
  }
];

// -------------------------------------------------
// Questions and Grading
// -------------------------------------------------

// Generate button for individual question and append it to question container
questions.map(question => {
  const { id } = question;
  $(".question-container").before(
    `<button class="question btn btn-dark" data-id=${id}>Q${id}</button>`
  );
});

// Create a global variable for currently selected question
let currentQ = 0;

// Attach an onClick to individual question buttons
$(".question-marker").on("click", "button.question", function() {
  // Get id from data-id attribute
  const id = $(this).attr("data-id");

  // Remove active from all buttons
  $("button.question").removeClass("active");
  // Add class active to currentQ
  $(this).addClass("active");

  // Set global currentQ to current question
  currentQ = id;

  // Get question details by matching currentQ variable
  const { question, video_link } = questions.filter(data => data.id == id)[0];

  // Change question title
  $(".question-title").text(`Q${id}: ${question}`);
  // Change video src
  $("#youtube-video").attr("src", video_link);

  // Loading previously entered data, if exists
  // Check if some data exists in localStorage
  if (localStorage.getItem("gradeData")) {
    const data = JSON.parse(localStorage.getItem("gradeData"));
    const result = data.filter(item => item.id == id);
    if (result.length === 1) {
      // Data exists... get it back
      const { grade, comment } = result[0];
      $(`input[name='grade'][value=${grade}]`).prop("checked", true);
      $("#comment-box").val(comment);
    } else {
      // Empty the textarea and unselect radio button
      $("input[name='grade']").prop("checked", false);
      $("#comment-box").val("");
    }
  } else {
    // Empty the textarea and unselect radio button
    $("input[name='grade']").prop("checked", false);
    $("#comment-box").val("");
  }
});

// Function for previous and next button
$("#question-prev").on("click", function() {
  if (currentQ > 0) {
    currentQ--;
    $(`button[data-id=${currentQ}]`).trigger("click");
  }
});
$("#question-next").on("click", function() {
  if (currentQ < questions.length) {
    currentQ++;
    $(`button[data-id=${currentQ}]`).trigger("click");
  } else if (currentQ == questions.length) {
    alert("This was last question. Redirecting to Results");
    $(location).attr("href", "result.html");
  }
});

// Save the data entered for individual question
$("#save-answer").on("click", function() {
  if (currentQ == 0) {
    alert("Please select a question first!");
  } else {
    // Get the grading value
    const grade = $("input[name='grade']:checked").val();
    if (!grade) {
      alert("Please choose a grade before saving");
      return;
    }
    // Get comment
    const comment = $("#comment-box").val();

    const answerObj = {
      id: currentQ,
      grade,
      comment
    };

    // Check if localstorage exists
    if (localStorage.getItem("gradeData")) {
      // Get the previously stored value
      const initData = JSON.parse(localStorage.getItem("gradeData"));

      // Checking if current answer exists in stored data
      // Create an array without the repeated value (if exists)
      const checkArr = initData.filter(item => item.id != answerObj.id);

      // Append answerObj to previously stored data using spread operator
      localStorage.setItem(
        "gradeData",
        JSON.stringify([...checkArr, answerObj])
      );
    } else {
      // Create a localStorage item and push current object to it
      const ans = [answerObj];
      localStorage.setItem("gradeData", JSON.stringify(ans));
    }
  }
});

// Reset button
$("#reset-answer").on("click", () => {
  if (localStorage.getItem("gradeData")) {
    const data = JSON.parse(localStorage.getItem("gradeData"));
    const updatedData = data.filter(item => item.id != currentQ);
    localStorage.setItem("gradeData", JSON.stringify(updatedData));
    // Empty the textarea and unselect radio button
    $("input[name='grade']").prop("checked", false);
    $("#comment-box").val("");
  }
});

// Reset all
$("#reset-all").on("click", () => {
  let x = confirm("Are you sure? This will delete all stored answers.");
  if (x) {
    localStorage.removeItem("gradeData");
  }
  // Empty the textarea and unselect radio button
  $("input[name='grade']").prop("checked", false);
  $("#comment-box").val("");
});
