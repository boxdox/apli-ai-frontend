// Check if data exists in localStorage
if (localStorage.getItem("gradeData")) {
  const data = JSON.parse(localStorage.getItem("gradeData"));
  const sortedData = data.sort(function(a, b) {
    return a.id - b.id;
  });
  sortedData.map(function(item) {
    const { id, grade, comment } = item;
    $("tbody").before(`
        <tr>
            <th scope="row">${id}</th>
            <td>${grade}/5</td>
            <td>${comment}</td>
        </tr>
    `);
  });
} else {
  $("table").hide();
  $("h1").after('<h4 class="text-center">No data found</h4>');
}

$("#go-back").on("click", () => $(location).attr("href", "index.html"));
